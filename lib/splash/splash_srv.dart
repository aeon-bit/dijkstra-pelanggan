import 'dart:async';

import 'package:dijkstra_pelanggan/main.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:async/async.dart';

import '../routes/app_pages.dart';

class SplashSrv extends GetxService {
  final _memo = AsyncMemoizer<void>();

  Future<void> init() {
    return _memo.runOnce(_initFunction);
  }
}

Future<void> _initFunction() async {
  await Future.delayed(Duration(seconds: 2));
  // Get.toNamed(Routes.HOME);
  // print('DONE INIT');
  //cancel the timer once we are done
}

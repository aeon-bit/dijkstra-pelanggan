import 'package:dijkstra_pelanggan/splash/splash_ctr.dart';
import 'package:dijkstra_pelanggan/splash/splash_srv.dart';
import 'package:dijkstra_pelanggan/widgets/app_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

import '../widgets/app_widgets.dart';

class SplashView extends GetView<SplashCtr> {
  SplashView({super.key});

  final _appStyles = AppStyles();
  final _appWidgets = AppWidgets();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.restaurant_menu_rounded,
              color: _appStyles.primColor,
              size: 100,
            ),
            const SizedBox(
              height: 12,
            ),
            _appWidgets.gMontserrat(
              text: 'Kedai Makan',
              size: 22,
              color: _appStyles.primColor,
              fontWeight: FontWeight.bold,
              textAlign: TextAlign.center,
            ),
            _appWidgets.gMontserrat(
              text: 'Bu Nur',
              size: 28,
              color: _appStyles.primColor,
              fontWeight: FontWeight.bold,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}

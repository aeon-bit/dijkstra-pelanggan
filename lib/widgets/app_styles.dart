import 'package:dijkstra_pelanggan/widgets/app_color.dart';
import 'package:flutter/material.dart';

class Pallete {
  static const MaterialColor primMatColor = MaterialColor(
    0xFF24BADA, // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
    <int, Color>{
      50: Color(0xFFF5FCFE),
      100: Color(0xFFEAF9FC),
      200: Color(0xFFC9EEF6),
      300: Color(0xFFA6E3F0),
      400: Color(0xFF66CFE6),
      500: Color(0xFF24BADA),
      600: Color(0xFF21A6C3),
      700: Color(0xFF167083),
      800: Color(0xFF115463),
      900: Color(0xFF0B3640),
    },
  );
}

class AppStyles {
  final primColor = const Color(0xFF24BADA);
  final errorColor = Color.fromARGB(255, 250, 104, 104);
  final textColor = const Color.fromARGB(255, 16, 16, 16);
  final textSecondColor = const Color.fromARGB(255, 75, 75, 75);

  final textStyleForm = TextStyle(
    color: AppColors().textSecondColor,
    fontFamily: 'Montserrat',
    fontSize: 12,
  );
  final shadowLargeInputPrimary = BoxShadow(
    color: const Color(0xFF24BADA).withOpacity(0.1),
    // spreadRadius: 5,
    blurRadius: 20,
    offset: const Offset(0, 5),
  );

  ///SHADOW
  final primBoxShadow = BoxShadow(
    color: AppColors().primColor.withOpacity(0.2),
    blurRadius: 7.0,
    spreadRadius: 2.0,
    offset: Offset(0.0, 5.0),
  );
}

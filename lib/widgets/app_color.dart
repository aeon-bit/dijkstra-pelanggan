import 'package:flutter/material.dart';

class AppColors {
  final primColor = const Color(0xFF24BADA);
  final textColor = Color.fromARGB(255, 16, 16, 16);
  final textSecondColor = Color.fromARGB(255, 75, 75, 75);
}

import 'package:dijkstra_pelanggan/widgets/app_color.dart';
import 'package:dijkstra_pelanggan/widgets/app_styles.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppWidgets {
  Text gMontserrat({
    required String text,
    double size = 12.0,
    Color color = Colors.black87,
    FontWeight fontWeight = FontWeight.normal,
    TextAlign textAlign = TextAlign.left,
  }) {
    return Text(
      text,
      style: GoogleFonts.getFont(
        'Montserrat',
        color: color,
        fontWeight: fontWeight,
        fontSize: size,
      ),
      textAlign: textAlign,
      overflow: TextOverflow.ellipsis,
    );
  }
}

// class gFontMontserrat extends StatelessWidget {
//   const gFontMontserrat({
//     super.key,
//     this.text = '',
//     this.fontSize = 12,
//     this.color = Colors.black87,
//     this.fontWeight = FontWeight.normal,
//     this.textAlign = TextAlign.left,
//   });

//   final String text;
//   final double fontSize;
//   final Color color;
//   final FontWeight fontWeight;
//   final TextAlign textAlign;

//   @override
//   Widget build(BuildContext context) {
//     return Text(
//       text,
//       style: GoogleFonts.getFont(
//         'Montserrat',
//         fontSize: fontSize,
//         color: color,
//         fontWeight: fontWeight,
//       ),
//     );
//   }
// }

class AppBarCustom extends StatelessWidget {
  const AppBarCustom({
    Key? key,
    required this.scWidth,
    required AppWidgets appWidgets,
    required AppStyles appStyles,
    required String title,
  })  : _appWidgets = appWidgets,
        _appStyles = appStyles,
        _title = title,
        super(key: key);

  final double scWidth;
  final AppWidgets _appWidgets;
  final AppStyles _appStyles;
  final String _title;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: scWidth,
      height: AppBar().preferredSize.height,
      margin: const EdgeInsets.only(bottom: 24),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _appWidgets.gMontserrat(
            text: _title,
            fontWeight: FontWeight.bold,
            color: _appStyles.primColor,
            size: 20,
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }
}

class BtnLarge extends StatelessWidget {
  BtnLarge({
    super.key,
    required this.scWidth,
    required AppColors appColors,
    required AppStyles appStyles,
    required String title,
    required Color color,
  })  : _appColors = appColors,
        _appStyles = appStyles,
        _color = color,
        _title = title;

  final double scWidth;
  final AppColors _appColors;
  final AppStyles _appStyles;
  final String _title;
  final Color _color;

  final _appWidgets = AppWidgets();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: scWidth,
      height: 45,
      decoration: BoxDecoration(
        color: _color,
        borderRadius: BorderRadius.circular(12),
        boxShadow: [_appStyles.shadowLargeInputPrimary],
      ),
      child: Center(
        child: _appWidgets.gMontserrat(
          text: _title,
          fontWeight: FontWeight.bold,
          color: Colors.white,
        ),
      ),
    );
  }
}

class FormLarge_prefix extends StatelessWidget {
  const FormLarge_prefix({
    super.key,
    required this.scWidth,
    required AppStyles appStyles,
    required AppColors appColors,
    required String hint,
    required IconData prefixIcon,
  })  : _appStyles = appStyles,
        _appColors = appColors,
        _hint = hint,
        _prefixIcon = prefixIcon;

  final double scWidth;
  final AppStyles _appStyles;
  final AppColors _appColors;

  final String _hint;
  final IconData _prefixIcon;

  @override
  Widget build(BuildContext context) {
    return Container(
      // alignment: Alignment.center,
      width: scWidth,
      height: 45,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
        boxShadow: [
          _appStyles.shadowLargeInputPrimary,
        ],
      ),
      child: TextField(
        decoration: InputDecoration(
            hintText: _hint,
            hintStyle: _appStyles.textStyleForm,
            enabledBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
            prefixIcon: Icon(
              _prefixIcon,
              size: 18,
              color: _appColors.textSecondColor,
            )),
        cursorHeight: 14,
      ),
    );
  }
}

class FormLarge_password extends StatelessWidget {
  const FormLarge_password({
    super.key,
    required this.scWidth,
    required AppStyles appStyles,
    required AppColors appColors,
    required String hint,
    required IconData prefixIcon,
    suffixIcon,
  })  : _appStyles = appStyles,
        _appColors = appColors,
        _hint = hint,
        _prefixIcon = prefixIcon,
        _suffixIcon = suffixIcon;

  final double scWidth;
  final AppStyles _appStyles;
  final AppColors _appColors;

  final String _hint;
  final IconData _prefixIcon, _suffixIcon;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: scWidth,
      height: 45,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
        boxShadow: [
          _appStyles.shadowLargeInputPrimary,
        ],
      ),
      child: TextField(
        obscureText: true,
        decoration: InputDecoration(
          hintText: _hint,
          hintStyle: _appStyles.textStyleForm,
          enabledBorder: InputBorder.none,
          focusedBorder: InputBorder.none,
          prefixIcon: Icon(
            _prefixIcon,
            size: 18,
            color: _appColors.textSecondColor,
          ),
          suffixIcon: Icon(
            _suffixIcon,
            size: 18,
            color: _appColors.textSecondColor,
          ),
        ),
        cursorHeight: 14,
      ),
    );
  }
}

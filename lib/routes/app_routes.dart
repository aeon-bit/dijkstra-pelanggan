part of 'app_pages.dart';

abstract class Routes {
  static const SPLASH = _Paths.SPLASH;
  static const LOGIN = _Paths.LOGIN;
  static const HOME = _Paths.HOME;

  static const DAFTAR_MENU = _Paths.HOME + _Paths.DAFTAR_MENU;
  static const PROFILE = _Paths.HOME + _Paths.PROFILE;
}

abstract class _Paths {
  static const SPLASH = '/splash';
  static const LOGIN = '/login';
  static const HOME = '/home';

  static const DAFTAR_MENU = '/daftar_menu';
  static const PROFILE = '/profile';
}

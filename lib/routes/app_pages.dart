import 'package:dijkstra_pelanggan/home/home_view.dart';
import 'package:dijkstra_pelanggan/home/profile/profile_view.dart';
import 'package:dijkstra_pelanggan/root/root_view.dart';
import 'package:dijkstra_pelanggan/splash/splash_view.dart';
import 'package:get/get.dart';

import '../auth/login_bind.dart';
import '../auth/login_view.dart';
import '../home/daftar_menu/daftarmenu_bind.dart';
import '../home/daftar_menu/daftarmenu_view.dart';
import '../home/home_bind.dart';
import '../home/profile/profile_bind.dart';
import '../root/root_bind.dart';
import '../splash/splash_bind.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASH;
  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBind(),
      children: [
        GetPage(
          name: _Paths.DAFTAR_MENU,
          page: () => DaftarMenuView(),
          binding: DaftarMenuBind(),
        ),
        GetPage(
          name: _Paths.PROFILE,
          page: () => ProfileView(),
          binding: ProfileBind(),
        ),
      ],
    ),
    GetPage(
      name: _Paths.SPLASH,
      page: () => SplashView(),
      binding: SplashBind(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBind(),
    ),
  ];

  // static const INITIAL = Routes.HOME;
  // static final routes = [
  //   GetPage(
  //     name: '/',
  //     page: () => RootView(),
  //     binding: RootBind(),
  //     participatesInRootNavigator: true,
  //     preventDuplicates: true,
  //     children: [
  //       GetPage(
  //         name: _Paths.LOGIN,
  //         page: () => LoginView(),
  //         binding: LoginBind(),
  //       ),
  //       GetPage(
  //         name: _Paths.LOGIN,
  //         page: () => LoginView(),
  //         binding: LoginBind(),
  //       ),
  //       GetPage(
  //         name: _Paths.HOME,
  //         page: () => HomeView(),
  //         preventDuplicates: true,
  //         children: [
  //           GetPage(
  //             name: _Paths.DAFTAR_MENU,
  //             page: () => DaftarMenuView(),
  //             binding: DaftarMenuBind(),
  //           ),
  //         ],
  //       ),
  //     ],
  //   ),
  // ];
}

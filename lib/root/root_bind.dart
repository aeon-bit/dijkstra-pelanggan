import 'package:dijkstra_pelanggan/root/root_ctr.dart';
import 'package:get/get.dart';

class RootBind extends Bindings {
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.lazyPut<RootCtr>(() => RootCtr());
  }
}

import 'package:dijkstra_pelanggan/auth/login_ctr.dart';
import 'package:dijkstra_pelanggan/widgets/app_widgets.dart';
import 'package:dijkstra_pelanggan/widgets/app_color.dart';
import 'package:dijkstra_pelanggan/widgets/app_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginView extends GetView<LoginCtr> {
  LoginView({super.key});

  final _appStyles = AppStyles();
  final _appWidgets = AppWidgets();
  final _appColors = AppColors();

  @override
  Widget build(BuildContext context) {
    final double scWidth = MediaQuery.of(context).size.width;
    return MaterialApp(
      home: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                children: [
                  Icon(
                    Icons.ac_unit_rounded,
                    color: _appStyles.primColor,
                    size: 100,
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _appWidgets.gMontserrat(
                        text: 'Login',
                        size: 24,
                        color: _appStyles.primColor,
                        fontWeight: FontWeight.bold,
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  FormLarge_prefix(
                    scWidth: scWidth,
                    appStyles: _appStyles,
                    appColors: _appColors,
                    hint: 'Username',
                    prefixIcon: Icons.person_rounded,
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  FormLarge_password(
                    scWidth: scWidth,
                    appStyles: _appStyles,
                    appColors: _appColors,
                    hint: 'Password',
                    prefixIcon: Icons.lock_rounded,
                    suffixIcon: Icons.visibility_off_rounded,
                  ),
                  const SizedBox(
                    height: 23,
                  ),
                  InkWell(
                    onTap: () {
                      controller.login();
                    },
                    child: BtnLarge(
                      title: 'Login',
                      scWidth: scWidth,
                      appColors: _appColors,
                      appStyles: _appStyles,
                      color: _appStyles.primColor,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:dijkstra_pelanggan/auth/login_view.dart';
import 'package:dijkstra_pelanggan/routes/app_pages.dart';
import 'package:dijkstra_pelanggan/splash/splash_srv.dart';
import 'package:dijkstra_pelanggan/splash/splash_view.dart';
import 'package:dijkstra_pelanggan/widgets/app_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';

void main() async {
  await GetStorage.init();
  runApp(
    GetMaterialApp(
      title: "Application",
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
      debugShowCheckedModeBanner: false,
    ),

    // GetMaterialApp.router(
    //   debugShowCheckedModeBanner: false,
    //   theme: ThemeData(
    //     primarySwatch: Pallete.primMatColor,
    //     // textTheme: GoogleFonts.getFont('Montserrat'),
    //   ),
    //   title: 'Dijkstra-Pelanggan',
    //   initialBinding: BindingsBuilder(
    //     () {
    //       Get.put(
    //         SplashSrv(),
    //       );
    //     },
    //   ),
    //   builder: (context, child) {
    //     final _appStyles = AppStyles();
    //     return FutureBuilder(
    //       key: ValueKey('initFuture'),
    //       future: SplashSrv().init(),
    //       builder: (context, snapshot) {
    //         if (snapshot.connectionState == ConnectionState.done) {
    //           // return LoginView();
    //           // return child ?? SizedBox.shrink();
    //           Get.toNamed(Routes.HOME);
    //         }
    //         // } else {
    //         return SplashView();
    //         // }
    //       },
    //     );
    //   },
    // ),
  );
}

// Future<void> timer() async {
//   await Future.delayed(const Duration(milliseconds: 3000));
//   print('Operasi selesai');
// }

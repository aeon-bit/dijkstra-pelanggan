import 'package:dijkstra_pelanggan/home/daftar_menu/daftarmenu_view.dart';
import 'package:dijkstra_pelanggan/home/home_ctr.dart';
import 'package:dijkstra_pelanggan/home/profile/profile_view.dart';
import 'package:dijkstra_pelanggan/widgets/app_styles.dart';
import 'package:dijkstra_pelanggan/widgets/app_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';

import '../routes/app_pages.dart';

// class HomeView extends StatelessWidget {
//   const HomeView({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return GetRouterOutlet.builder(
//       builder: (context, delegate, currentRoute) {
//         return Scaffold(
//           body: GetRouterOutlet(
//             initialRoute: Routes.DAFTAR_MENU,
//             key: Get.nestedKey(Routes.HOME),
//           ),
//         );
//       },
//     );
//   }
// }

class HomeView extends GetView<HomeCtr> {
  HomeView({super.key});

  final _appWidgets = AppWidgets();
  final _appStyle = AppStyles();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () => IndexedStack(
          index: controller.currentIndexTab.value,
          children: [
            DaftarMenuView(),
            ProfileView(),
          ],
        ),
      ),
      bottomNavigationBar: Obx(
        () => SalomonBottomBar(
          currentIndex: controller.currentIndexTab.value,
          items: [
            SalomonBottomBarItem(
              icon: const Icon(Icons.local_restaurant_rounded),
              title: _appWidgets.gMontserrat(
                text: 'Daftar Menu',
              ),
              selectedColor: _appStyle.primColor,
            ),
            SalomonBottomBarItem(
              icon: const Icon(Icons.person_rounded),
              title: _appWidgets.gMontserrat(
                text: 'Profile',
              ),
              selectedColor: _appStyle.primColor,
            ),
          ],
          onTap: (value) {
            controller.currentIndexTab.value = value;
          },
        ),
      ),
    );
  }
}

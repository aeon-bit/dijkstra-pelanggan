import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class DaftarMenuCtr extends GetxController {
  List<NetworkImage> listMakanan = [
    const NetworkImage('https://loremflickr.com/320/240/food'),
    const NetworkImage('https://loremflickr.com/320/240/food'),
    const NetworkImage('https://loremflickr.com/320/240/food'),
    const NetworkImage('https://loremflickr.com/320/240/food'),
    const NetworkImage('https://loremflickr.com/320/240/food'),
  ];
  List<String> listHarga = [
    '9.500,-',
    '12.000,-',
    '6.000,-',
    '7.000,-',
    '8.000,-',
    '13.500,-',
    '21.000,-',
    '24.000,-',
  ];
}

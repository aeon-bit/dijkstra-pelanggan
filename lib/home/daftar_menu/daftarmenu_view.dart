import 'dart:math';
import 'dart:ui';

import 'package:dijkstra_pelanggan/home/daftar_menu/daftarmenu_ctr.dart';
import 'package:dijkstra_pelanggan/widgets/app_styles.dart';
import 'package:dijkstra_pelanggan/widgets/app_widgets.dart';
import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DaftarMenuView extends GetView<DaftarMenuCtr> {
  DaftarMenuView({super.key});

  final _ctr = Get.lazyPut<DaftarMenuCtr>(() => DaftarMenuCtr());

  final _appWidgets = AppWidgets();
  final _appStyles = AppStyles();
  final _faker = Faker();
  final _random = Random();

  @override
  Widget build(BuildContext context) {
    final double scWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            AppBarCustom(
              scWidth: scWidth,
              appWidgets: _appWidgets,
              appStyles: _appStyles,
              title: 'Daftar Menu',
            ),
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _appWidgets.gMontserrat(
                      text: 'Menu Makanan',
                      fontWeight: FontWeight.bold,
                      color: _appStyles.textColor,
                      size: 14,
                      textAlign: TextAlign.left,
                    ),
                    Row(
                      children: [
                        _appWidgets.gMontserrat(
                          text: 'Selengkapnya',
                          fontWeight: FontWeight.normal,
                          color: _appStyles.primColor,
                          size: 12,
                          textAlign: TextAlign.left,
                        ),
                        Icon(
                          Icons.keyboard_double_arrow_right_rounded,
                          size: 14,
                          color: _appStyles.primColor,
                        )
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 12,
                ),
                LIstMenuMakanan(
                    scWidth: scWidth,
                    appStyles: _appStyles,
                    controller: controller,
                    random: _random,
                    appWidgets: _appWidgets,
                    faker: _faker),
                const SizedBox(
                  height: 32,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _appWidgets.gMontserrat(
                      text: 'Menu Minuman',
                      fontWeight: FontWeight.bold,
                      color: _appStyles.textColor,
                      size: 14,
                      textAlign: TextAlign.left,
                    ),
                    Row(
                      children: [
                        _appWidgets.gMontserrat(
                          text: 'Selengkapnya',
                          fontWeight: FontWeight.normal,
                          color: _appStyles.primColor,
                          size: 12,
                          textAlign: TextAlign.left,
                        ),
                        Icon(
                          Icons.keyboard_double_arrow_right_rounded,
                          size: 14,
                          color: _appStyles.primColor,
                        )
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 12,
                ),
                LIstMenuMakanan(
                    scWidth: scWidth,
                    appStyles: _appStyles,
                    controller: controller,
                    random: _random,
                    appWidgets: _appWidgets,
                    faker: _faker),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class LIstMenuMakanan extends StatelessWidget {
  const LIstMenuMakanan({
    Key? key,
    required this.scWidth,
    required AppStyles appStyles,
    required this.controller,
    required Random random,
    required AppWidgets appWidgets,
    required Faker faker,
  })  : _appStyles = appStyles,
        _random = random,
        _appWidgets = appWidgets,
        _faker = faker,
        super(key: key);

  final double scWidth;
  final AppStyles _appStyles;
  final DaftarMenuCtr controller;
  final Random _random;
  final AppWidgets _appWidgets;
  final Faker _faker;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: scWidth,
      height: 180,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 5,
        clipBehavior: Clip.none,
        itemBuilder: (context, index) {
          return Container(
            width: scWidth / 3 - 10,
            height: 200,
            margin: const EdgeInsets.only(right: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [_appStyles.primBoxShadow],
              image: DecorationImage(
                image: controller.listMakanan[_random.nextInt(5)],
                fit: BoxFit.cover,
              ),
            ),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: double.infinity,
                height: 60,
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.5),
                  borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          _appWidgets.gMontserrat(
                            text: _faker.food.restaurant(),
                            color: Colors.white,
                            size: 14,
                            fontWeight: FontWeight.bold,
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          _appWidgets.gMontserrat(
                            text: controller.listHarga[_random.nextInt(8)],
                            color: Colors.white,
                            size: 12,
                            fontWeight: FontWeight.normal,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

class LIstMenuMinuman extends StatelessWidget {
  const LIstMenuMinuman({
    Key? key,
    required this.scWidth,
    required AppStyles appStyles,
    required this.controller,
    required Random random,
    required AppWidgets appWidgets,
    required Faker faker,
  })  : _appStyles = appStyles,
        _random = random,
        _appWidgets = appWidgets,
        _faker = faker,
        super(key: key);

  final double scWidth;
  final AppStyles _appStyles;
  final DaftarMenuCtr controller;
  final Random _random;
  final AppWidgets _appWidgets;
  final Faker _faker;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: scWidth,
      height: 180,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 5,
        clipBehavior: Clip.none,
        itemBuilder: (context, index) {
          return Container(
            width: scWidth / 3 - 10,
            height: 200,
            margin: const EdgeInsets.only(right: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [_appStyles.primBoxShadow],
              image: DecorationImage(
                image: controller.listMakanan[_random.nextInt(5)],
                fit: BoxFit.cover,
              ),
            ),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                width: double.infinity,
                height: 60,
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.5),
                  borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          _appWidgets.gMontserrat(
                            text: _faker.food.restaurant(),
                            color: Colors.white,
                            size: 14,
                            fontWeight: FontWeight.bold,
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          _appWidgets.gMontserrat(
                            text: controller.listHarga[_random.nextInt(8)],
                            color: Colors.white,
                            size: 12,
                            fontWeight: FontWeight.normal,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../routes/app_pages.dart';

class ProfileCtr extends GetxController {
  final getStorage = GetStorage();

  performLogout() {
    getStorage.erase();
    Get.offNamed(Routes.LOGIN);
  }
}

import 'package:dijkstra_pelanggan/home/profile/profile_ctr.dart';
import 'package:dijkstra_pelanggan/widgets/app_color.dart';
import 'package:faker/faker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../widgets/app_styles.dart';
import '../../widgets/app_widgets.dart';

class ProfileView extends GetView<ProfileCtr> {
  ProfileView({super.key});

  final _ctr = Get.put(ProfileCtr());

  final _appWidgets = AppWidgets();
  final _appStyles = AppStyles();
  final _appColors = AppColors();
  final _faker = Faker();

  @override
  Widget build(BuildContext context) {
    final double scWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            AppBarCustom(
              scWidth: scWidth,
              appWidgets: _appWidgets,
              appStyles: _appStyles,
              title: 'Profil Pengguna',
            ),
            Column(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(20),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                          boxShadow: [_appStyles.primBoxShadow]),
                      child: Icon(
                        Icons.person_rounded,
                        size: 100,
                        color: _appStyles.primColor,
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        _appWidgets.gMontserrat(
                          text: 'Ubah Foto Profil',
                          size: 16,
                          fontWeight: FontWeight.w600,
                          color: _appStyles.textSecondColor,
                        ),
                        IconButton(
                            onPressed: () {},
                            icon: Icon(
                              Icons.edit_outlined,
                              color: _appStyles.textSecondColor,
                            )),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 24,
                ),
                Column(
                  children: [
                    _lineDataProfile(
                        'ID Pelanggan',
                        _faker.randomGenerator
                            .fromPattern(['#####', '##-####'])),
                    _lineDataProfile('Nama Lengkap', _faker.person.name()),
                    _lineDataProfile('No Telepon', _faker.phoneNumber.us()),
                    _lineDataProfile(
                        'Username',
                        _faker.person.firstName() +
                            _faker.randomGenerator.fromCharSet(
                                "ABCDEFGHIJKLMONPQESTUVWXYZ!?@#", 3)),
                    _lineDataProfile('Password', '********'),
                  ],
                ),
                const SizedBox(
                  height: 24,
                ),
                BtnLarge(
                  scWidth: scWidth,
                  appColors: _appColors,
                  appStyles: _appStyles,
                  title: 'Ubah Data',
                  color: _appStyles.primColor,
                ),
                const SizedBox(
                  height: 12,
                ),
                InkWell(
                  onTap: () {
                    controller.performLogout();
                  },
                  child: BtnLarge(
                    scWidth: scWidth,
                    appColors: _appColors,
                    appStyles: _appStyles,
                    title: 'Logout',
                    color: _appStyles.errorColor,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Container _lineDataProfile(String title, String data) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _appWidgets.gMontserrat(
            text: title,
            color: _appStyles.textColor,
            fontWeight: FontWeight.bold,
            textAlign: TextAlign.left,
          ),
          _appWidgets.gMontserrat(
            text: data,
            color: _appStyles.primColor,
            fontWeight: FontWeight.bold,
            textAlign: TextAlign.right,
          ),
        ],
      ),
    );
  }
}
